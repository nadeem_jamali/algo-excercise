import sys
import csv

def get_empty_list(size:int):
    return [0 for i in range(size)]


def get_list_of_list(outer_size:int, inner_size:int):
    return [get_empty_list(inner_size) for s in range(outer_size)]


def optimal_bst(p, q, n: int):
    e = get_list_of_list(n+1, n)
    w = get_list_of_list(n+1, n)
    root = get_list_of_list(n, n)

    for i in range(1, n+1):
        e[i][i-1] = q[i-1]
        w[i][i-1] = q[i-1]

    for el in range(1, n):
        for i in range(1, n-el+1):
            j = i + el - 1
            e[i][j] = sys.maxsize
            w[i][j] = w[i][j-1] + p[j] + q[j]
            for r in range(i, j+1):
                t = e[i][r - 1] + e[r + 1][j] + w[i][j]
                if t < e[i][j]:
                    e[i][j] = t
                    root[i][j] = r
    return e, w, root


def save_csv(table, name):    
    with open('sol_%s.csv' % name, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(table)


def print_table(table, name):
    print('==' * len(table[0]))
    print('table: %s' % name)
    print('==' * len(table[0]))
    for row in table:
        for col in row:
            print('\t%s\t|' % col, end='')
            print('\n')
            print('\t--\t' * len(row))
            print('==' * len(table[0]))


# example
# p = [None, 0.15, 0.10, 0.05, 0.10, 0.20]
# q = [0.05, 0.10, 0.05, 0.05, 0.05, 0.10]

p = [None, 0.10, 0.04, 0.12, 0.10, 0.18]
q = [0.08, 0.06, 0.08, 0.10, 0.04, 0.10]

e, w, root = optimal_bst(p, q, 6)

# print_table(e, 'e')
# print_table(w, 'w')
# print_table(root, 'root')

save_csv(e, 'e')
save_csv(w, 'w')
save_csv(root, 'root')