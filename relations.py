"""
Producing relations of sequences.
"""

def generate_binary_relations(s1, s2):
    """
    Generats binary relations of the given sequences in s1 x s1 order.
    Expects s1 and s2 as two tupples.
    Returns a list of tupples.
    """
    result = []
    for e1 in s1:
        for e2 in s2:
            p = (e1, e2)
            result.append(p)

    return result

S1 = (1, 2, 3)
S2 = ('a', 'b')

R = generate_binary_relations(S1, S2)
print('Length of R: %s' % len(R))
print(R)
